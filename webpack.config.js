var path = require("path");
var webpack = require('webpack');
var BundleTracker = require('webpack-bundle-tracker');

module.exports = {
    context: __dirname,

    entry: 
    {
        'polyfill': 'babel-polyfill',
        'main': './DjangoReactTest/landing/static/js/main/index',
        'login': './DjangoReactTest/landing/static/js/login/index',
        'register': './DjangoReactTest/landing/static/js/register/index'
    },
    
    output: {
        path: path.resolve('./DjangoReactTest/landing/static/bundles/'),
        filename: "[name]-[hash].js",
    },

    plugins: [
        new BundleTracker({ filename: './DjangoReactTest/webpack-stats.json' }),
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            }
        ]
    },
    resolve: {
        extensions: ['*', '.js', '.jsx']
    }

};