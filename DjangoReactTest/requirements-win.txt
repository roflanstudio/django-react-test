django >= 2.1.6, < 3.0
django-webpack-loader
djangorestframework
markdown
django-filter
djangorestframework-jwt
psycopg2
django-rest-framework-social-oauth2