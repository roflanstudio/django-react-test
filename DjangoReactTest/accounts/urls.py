from django.urls import re_path, include

from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

from .views import RegisterApiView,ConvertTokenApiView 

urlpatterns = [
    re_path(r'^register/$',RegisterApiView.as_view()),
    re_path(r'^convert/$',ConvertTokenApiView.as_view()),
    re_path(r'^$', obtain_jwt_token),
    re_path(r'^o/', include('rest_framework_social_oauth2.urls')),
    #re_path(r'^token/refresh/$', refresh_jwt_token),
]
