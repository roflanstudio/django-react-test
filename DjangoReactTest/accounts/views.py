from django.db.models import Q
from django.contrib.auth.models import User

from rest_framework.views import APIView
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework_jwt.settings import api_settings

from .utils import jwt_response_payload_handler
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

class ConvertTokenApiView(APIView):
    def get(self,request,*args, **kwargs):
        print(request.user)
        payload = jwt_payload_handler(request.user)
        token = jwt_encode_handler(payload)
        return Response(jwt_response_payload_handler(token,request.user,request=request))

class RegisterApiView(APIView):
    permission_classes = [AllowAny]
    authentication_classes = []
    def post(self,request,*args, **kwargs):
        print(request.user)
        if request.user.is_authenticated:
            return Response({'detal':"Ви вже зареєстровані та авторизовані"},status = 400)
        username = request.data.get('username')
        email = request.data.get('email')
        password1 = request.data.get('password1')
        password2 = request.data.get('password2')
        
        qs = User.objects.filter(
            Q(username__exact=username)|
            Q(email__exact=email)
        )
        if password1 != password2:
            return Response({'detal':"Паролі не співпадають"},status = 400)
        if qs.exists():
            return Response({'detal':"Користувач з таким логіном або поштою вже зареєстрований"},status = 400)
        user = User.objects.create(
            username=username,
            email = email
        )
        user.set_password(password1)
        user.save()
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        return Response(jwt_response_payload_handler(token,user,request=request))
