from django.db import models
from django.contrib.auth.models import User


class Task(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    done = models.BooleanField(default=False)
    important = models.BooleanField(default=False)

    # regular = 'RG'
    # done = 'DN'
    # important = "IM"

    # STATUS_CHOICES = [
    #     (regular, 'Regular'),
    #     (done, 'Done'),
    #     (important, 'important'),
    # ]

    # status = models.CharField(
    #     max_length=2,
    #     choices=STATUS_CHOICES,
    #     default=regular
    # )

    updated = models.DateField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.username + ' ' + self.name