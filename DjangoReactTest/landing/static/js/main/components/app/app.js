import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import cookie from 'cookie';

import AppHeader from '../app-header/app-header';
import SearchPanel from '../search-panel/search-panel';
import TodoList from '../todo-list/todo-list';
import ItemStatusFilter from '../item-status-filter/item-status-filter';
import ItemAddForm from '../item-add-form/item-add-form';
import LogoutBtn from '../logout-btn/logout-btn';


import './app.css';
import { async } from 'q';

class App extends React.Component {

  constructor() {
    super();

    this.state = {
        items: [],
        filter: 'all',
        search: ''
      };

    this.getData();

    this.onItemAdded = this.onItemAdded.bind(this);
    this.toggleProperty = this.toggleProperty.bind(this);
    this.onToggleDone = this.onToggleDone.bind(this);
    this.onToggleImportant = this.onToggleImportant.bind(this); 
    this.onDelete = this.onDelete.bind(this);
    this.onFilterChange = this.onFilterChange.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);


  }

    sendGetRequest = () => {
      return new Promise(function (resolve,reject){
          var data = null;

          var xhr = new XMLHttpRequest();
          xhr.withCredentials = true;

          
        
          xhr.onerror = function () {
            reject({
                status: this.status,
                statusText: xhr.statusText
            });
          };

          xhr.addEventListener("readystatechange", function () {
            if (this.readyState === 4) {
              //console.log(this.responseText);
              let items = JSON.parse(this.responseText);
              //console.log(items);
              let newItems = [];
              for (let i = 0; i < items.length; i++) {
                  let obj = items[i];
                  //console.log(obj);
                  let newObj = {id: obj.id, label: obj.name, important: obj.important, done: obj.done};
                  //console.log(newObj);
                  newItems.push(newObj);
                  resolve(newItems);
            }
            }
          });

          let cookies = cookie.parse(document.cookie);
          //console.log(cookies.token);
          xhr.open("GET", "/api/");
          xhr.setRequestHeader("Authorization", "JWT " + cookies.token);
          xhr.setRequestHeader("Accept", "*/*");
          xhr.setRequestHeader("Cache-Control", "no-cache");
          xhr.setRequestHeader("cache-control", "no-cache");

          xhr.send(data);
          //console.log(xhr.status)
          //console.log(newItems);
          
      })
    }

    sendPutRequest = async(obj,id) =>{
      let cookies = cookie.parse(document.cookie);
      axios
      .put(`/api/${id}/`,obj,
        {
          headers: { Authorization: "JWT " + cookies.token, "Content-Type": "application/json" }
        })
    }

    getData =  async () => {     
            // let items = await axios.get(`/api/`)
            // console.log(items);
            // let newItems = []
            // for (let i = 0; i < items.data.length; i++) {
            //     let obj = items.data[i];
            //     console.log(obj);
            //     let newObj = {id: obj.id, label: obj.name, important: true ? obj.status == "IM" : false, done: true ? obj.status == "DN" : false};
            //     console.log(newObj);
            //     newItems.push(newObj);
            // }
          let newItems = await this.sendGetRequest();
          //console.log(newItems);
          this.setState({items:newItems});
          
        }

    onItemAdded(label, id) {
        this.setState((state) => {
            const item = this.createItem(label, id);
            //console.log(label, id, state.items, item)
            return { items: [...state.items, item] };
        })
        //console.log(this.state.items);
    }


  toggleProperty(arr, id, propName) {
    const idx = arr.findIndex((item) => item.id === id);
    const oldItem = arr[idx];
    const value = !oldItem[propName];

    const item = { ...arr[idx], [propName]: value } ;
    return [
      ...arr.slice(0, idx),
      item,
      ...arr.slice(idx + 1)
    ];
  };

  onToggleDone(item) {
    this.setState((state) => {
      const items = this.toggleProperty(state.items, item.id, 'done');
      //console.log(item);
      let obj = {
        name:item.label,
        done:!item.done,
        important: item.important
      }
      this.sendPutRequest(obj,item.id);
      return { items };
    });
  };

  onToggleImportant(item) {
    this.setState((state) => {
      const items = this.toggleProperty(state.items, item.id, 'important');
      let obj = {
        name:item.label,
        done:item.done,
        important: !item.important
      }
      this.sendPutRequest(obj,item.id);
      return { items };
    });
  };

  onDelete(id) {
    this.setState((state) => {
      const idx = state.items.findIndex((item) => item.id === id);
      const items = [
        ...state.items.slice(0, idx),
        ...state.items.slice(idx + 1)
      ];

      let cookies = cookie.parse(document.cookie);
      axios
        .delete(`/api/${id}/`,
          {
            headers: { Authorization: "JWT " + cookies.token, "Content-Type": "application/json" }
          })
        .then(res => {
          //console.log(res.data);
          })

      return { items };
    });
  };

  onFilterChange(filter) {
    this.setState({ filter });
  };

  onSearchChange(search) {
    this.setState({ search });
  };

  createItem(label, id) {
    return {
      id,
      label,
      important: false,
      done: false
    };
  }

  filterItems(items, filter) {
    if (filter === 'all') {
      return items;
    } else if (filter === 'active') {
      return items.filter((item) => (!item.done));
    } else if (filter === 'done') {
      return items.filter((item) => item.done);
    }
  }

  searchItems(items, search) {
    if (search.length === 0) {
      return items;
    }

    return items.filter((item) => {
      return item.label.toLowerCase().indexOf(search.toLowerCase()) > -1;
    });
  }
  

  render() {
    const { items, filter, search } = this.state;
    const doneCount = items.filter((item) => item.done).length;
    const toDoCount = items.length - doneCount;
    const visibleItems = this.searchItems(this.filterItems(items, filter), search);

    return (
      <div className="todo-app">
        <AppHeader toDo={toDoCount} done={doneCount}/>
        <div className="main-border">

          <div className="search-panel">
            <SearchPanel
              onSearchChange={this.onSearchChange}/>

            <ItemStatusFilter
              filter={filter}
              onFilterChange={this.onFilterChange} />
          </div>

          <TodoList
            items={ visibleItems }
            onToggleImportant={this.onToggleImportant}
            onToggleDone={this.onToggleDone}
            onDelete={this.onDelete} />

          <ItemAddForm
            onItemAdded={this.onItemAdded} />
          </div>
        <LogoutBtn/>
        
      </div>
    );
  };
}

export default App;