import React from 'react';
import axios from 'axios';
import cookie from 'cookie';

import './item-add-form.css';

class ItemAddForm extends React.Component {

  state = {
    label: '',
    id: 1231245
  };

  onLabelChange = (e) => {
    this.setState({
      label: e.target.value
    })
  };

  onSubmit = async (e) => {
    e.preventDefault();
    const { label } = this.state;
    this.setState({ label: '' });
    const cb = this.props.onItemAdded || (() => {});
 
    
    let item = {
      name:label,
      done: false,
      important: false
    }
    let cookies = cookie.parse(document.cookie);
    await axios
            .post(`/api/`, item, {
              headers: { Authorization: "JWT " + cookies.token, "Content-Type": "application/json" }
            })
            .then(res => {
                    //console.log(res.data.id);
                    cb(label, res.data.id);
            })
            .catch(error => {
              console.log(error)
              console.log(error.data)
                //TODO VALID
            })
    
    
  };

  render() {
    return (
      <form
        className="bottom-panel d-flex item-add-form"
        onSubmit={this.onSubmit}>

        <input type="text"
               className="form-control new-todo-label"
               value={this.state.label}
               onChange={this.onLabelChange}
               placeholder="What needs to be done?" />

        <button type="submit"
                className="btn btn-outline-info">Add</button>
      </form>
    );
  }
}

export default ItemAddForm;