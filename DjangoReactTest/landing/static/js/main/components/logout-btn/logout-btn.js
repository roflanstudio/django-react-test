import React from 'react';
import cookie from 'cookie';

import './logout-btn.css';

class LogoutBtn extends React.Component {

  state = {
    redirect: false
  }
  setRedirect = () => {
    this.setState({
      redirect: true
    })
  }
  renderRedirect = () => {
    if (this.state.redirect) {
        document.cookie = "token" + "=" + ";path= /" + ";expires=Thu, 01 Jan 1970 00:00:01 GMT";

        window.location.href = '/login/';
    }
  }
  render () {
    return (
       <div className=" row logout-btn-border  justify-content-between">
        <div className="col-6 txt">Press to logout.</div> 
        {this.renderRedirect()}
        <div className="col-4">
        <button 
            onClick={this.setRedirect}
            className="btn btn-outline-secondary logout-btn">Logout!</button>
        </div>
       </div>
    )
  }
}

export default LogoutBtn;