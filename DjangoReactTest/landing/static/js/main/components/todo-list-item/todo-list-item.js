import React from 'react';

import './todo-list-item.css';

const TodoListItem = ({ important, done,
      label, onToggleImportant, onToggleDone, onDelete }) => {

  let classNames = 'todo-list-item row';
  if (important) {
    classNames += ' important';
  }

  if (done) {
    classNames += ' done';
  }


  return (
    <div className={classNames}>
      <div
        className="todo-list-item-label col-8"
        onClick={onToggleDone}>{label}</div>

      <div className="col-4 todo-list-item-div">
        <button type="button"
                className="btn btn-outline-success btn-sm float-right"
                onClick={onToggleImportant}>
          <i className="fa fa-exclamation"></i>
        </button>

        <button type="button"
                className="btn btn-outline-danger btn-sm float-right"
                onClick={onDelete}>
          <i className="fa fa-trash-o"></i>
        </button>
      </div>
    </div>
  );
};

export default TodoListItem;