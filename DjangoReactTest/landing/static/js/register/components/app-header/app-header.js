import React from 'react';
import './app-header.css';

const AppHeader = () => {
  return (
    <div className="app-header d-flex">
      <h1>Sign up</h1>
    </div>
  );
};

export default AppHeader;