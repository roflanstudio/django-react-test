import React from 'react';
import axios from 'axios';

import './registration-form.css';

class RegistrationForm extends React.Component {


    state = {
        email: '',
        username: '',
        password: '',
        repeatPassword: ''
    };

    onEmailChange = (e) => {
        this.setState({
            email: e.target.value
        });
    };

    onLoginChange = (e) => {
        this.setState({
            username: e.target.value
        });
    };

    onPassChange = (e) => {
        this.setState({
            password: e.target.value
        });
    };

    onRepeatPassChange = (e) => {
        this.setState({
            repeatPassword: e.target.value
        });
    };

    onSubmit = async (e) => {
        e.preventDefault();
        const user = {
            username: this.state.username,
            email: this.state.email,
            password1: this.state.password,
            password2: this.state.repeatPassword
        }
        await axios
            .post(`/api/auth/register/`, user)
            .then(res => {
                    console.log(res.data);
                    document.cookie = `token=${res.data.token}; path=/; max-age=604800; samesite=lax`})
            .catch(error => {console.log(error)
                //TODO VALID
            })
        this.setState({
            email: '',
            username: '',
            password: '',
            repeatPassword: ''
        });
        window.location.href = "/";
        
    };

    render() {
        return(
            <form 
                className="bottom-panel registration-form"
                onSubmit={this.onSubmit}>
                
                <input type="email"
                    className="form-control new-todo-label"
                    value={this.state.email}
                    onChange={this.onEmailChange}
                    placeholder="Email" />
                <input type="login"
                    className="form-control new-todo-label"
                    value={this.state.username}
                    onChange={this.onLoginChange}
                    placeholder="Login" />
                <input type="password"
                    className="form-control new-todo-label"
                    value={this.state.password}
                    onChange={this.onPassChange}
                    placeholder="Password" />
                <input type="password"
                    className="form-control new-todo-label"
                    value={this.state.repeatPassword}
                    onChange={this.onRepeatPassChange}
                    placeholder="Repeat password" />
                <button type="submit"
                        className="btn btn-outline-info">Sign up</button>
                
            </form>
        );
    };
}

export default RegistrationForm