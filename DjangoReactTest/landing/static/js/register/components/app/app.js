import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

import AppHeader from '../app-header/app-header';

import './app.css';
import RegistrationForm from '../registration-form/registration-form'

class App extends React.Component {

    render() {
        return(
            <div className="registration-app">
                <AppHeader/>
                <RegistrationForm/>
            </div>
        );
    };
}

export default App;