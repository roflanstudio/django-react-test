import React from 'react';
import GoogleLogin from 'react-google-login';

import './google-btn.css';

class GoogleBtn extends React.Component {

    state = {
        django_client_id : "6W4g0hHQYDpw7RE4agMmJu94dGO1Wo94MRrjYdQp",
        django_client_secret : "gzyiu6q1CEHwHUWUMznWwnjv8tKfkTCO24g4t4Y21qfoQ7AZz9ZM8W82nX1CFfgzZCeXj32XCIuaQLKQIiaNSg94VsBa7sHcTV3LRU1y9zj1PzW9BUhV50iKy9MaXL3f"
    }

    convertGoogleToken(access_token) {
        const searchParams = new URLSearchParams();
    searchParams.set("grant_type", "convert_token");
    searchParams.set("client_id", this.state.django_client_id);
    searchParams.set("client_secret", this.state.django_client_secret);
    searchParams.set("backend", "google-oauth2");
    searchParams.set("token", access_token);

var xhr1 = new XMLHttpRequest();
xhr1.withCredentials = true;

xhr1.addEventListener("readystatechange", function () {
  if (this.readyState === 4) {
    var new_response = JSON.parse(this.responseText);
    console.log( new_response );
    //console.log(this);
    var xhr2 = new XMLHttpRequest();
    xhr2.open("GET", "/api/auth/convert/");
    xhr2.setRequestHeader("Authorization", "Bearer " + new_response.access_token);
    xhr2.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
          //console.log('JWT', this.responseText );
          var token = JSON.parse(this.responseText).token;
          document.cookie = `token=${token}; path=/; max-age=604800; samesite=lax`;
          window.location.href = '/';
        }
      });
    xhr2.send(searchParams);
  }
});

xhr1.open("POST", "/api/auth/o/convert-token/");
xhr1.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

xhr1.send(searchParams);

      }

    responseGoogle = (response) => {
        console.log(response);
        console.log(this.convertGoogleToken(response.Zi.access_token));
      }

    render() {
        return(
            <GoogleLogin
              clientId="512219384059-trb981vom2gm3vbtd2pb3g7rmc0tnapj.apps.googleusercontent.com"
              buttonText="Sign in with google"
              onSuccess={this.responseGoogle}
              onFailure={this.responseGoogle}
              cookiePolicy={'single_host_origin'}
            />
        );
    };
}

export default GoogleBtn;