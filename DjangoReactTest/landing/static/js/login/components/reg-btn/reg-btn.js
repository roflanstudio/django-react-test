import React from 'react';

import './reg-btn.css';

class RegBtn extends React.Component {
    render() {
        return(
            <div className="reg-btn-border flex-d row justify-content-between">
                <div className="col-7 txt">
                Don't have an account?</div>
                <div className="col-4">
                <a 
                    href="/register/"
                    className="btn btn-outline-secondary reg-btn">Sign up</a>
                </div>
            </div>

        );
    };
}

export default RegBtn;