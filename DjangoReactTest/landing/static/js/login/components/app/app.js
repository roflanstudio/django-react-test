import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';


import AppHeader from '../app-header/app-header';

import './app.css';
import AuthorizationForm from '../authorization-form/authorization-form';
import RegBtn from '../reg-btn/reg-btn';

class App extends React.Component {

    render() {
        return(
            <div className="login-app">
                <AppHeader/>
                <AuthorizationForm/>
                <RegBtn/>
            </div>
        );
    };
}

export default App;