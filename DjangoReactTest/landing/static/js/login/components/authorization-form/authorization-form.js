import React from 'react';
import axios from 'axios';
import cookie from 'cookie';

import './authorization-form.css';
import GoogleBtn from '../google-btn/google-btn';

class AuthorizationForm extends React.Component {

    state = {
        username: '',
        password: '',
    }

    onEmailChange = (e) => {
        this.setState({
            username: e.target.value
        });
    };

    onPassChange = (e) => {
        this.setState({
            password: e.target.value
        });
    };

    onSubmit = async (e) => {
        e.preventDefault();
        const user = {
            username: this.state.username,
            password: this.state.password
        }
        await axios
            .post(`/api/auth/`, user)
            .then(res => {
                    console.log(res.data.token);
                    document.cookie = `token=${res.data.token}; path=/; max-age=604800; samesite=lax`})
                    let cookies = cookie.parse(document.cookie);
                    console.log(cookies.token)
            
        
        // this.setState({
        //     username: '',
        //     password: ''
        // });
        window.location.href = '/';
    };

    



    render() {
        return(
            <form 
                className="bottom-panel authorization-form"
                onSubmit={this.onSubmit}>
                

                <input type="login"
                    className="form-control new-todo-label"
                    value={this.state.username}
                    onChange={this.onEmailChange}
                    placeholder="Login" />
                <input type="password"
                    className="form-control new-todo-label"
                    value={this.state.password}
                    onChange={this.onPassChange}
                    placeholder="Password" />
                <div className="row">
                <button type="submit"
                        className="btn btn-outline-info col-3">Sign in</button>
                <div className="google-btn col-1 or-btn">
                    or
                </div>
                <span className="col-8 google-btn">
                    <GoogleBtn/>    
                </span>
                </div>    
                
            </form>
        );
    };
}

export default AuthorizationForm