from django.shortcuts import render, redirect
from django.views.generic import View

from rest_framework.views import APIView
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly

from .models import Task
from .serializers import TaskSerializer

class FrontendView(View):
    def get(self, request, *args, **kwargs):
        try:
            request.COOKIES['token']
            return render(request,'index.html')
        except: 
            return redirect('/login/')       

class LoginView(View):
    def get(self, request, *args, **kwargs):
        try:
            request.COOKIES['token']
        except:
            return render(request,'login.html')
        return redirect('/')

class RegisterView(View):
    def get(self, request, *args, **kwargs):
        try:
            request.COOKIES['token']
        except:
            return render(request,'register.html')
        return redirect('/')
        

class TaskApiListView(ListCreateAPIView):
    #permission_classes = [IsAuthenticatedOrReadOnly]
    #authentication_classes = [SessionAuthentication]
    serializer_class = TaskSerializer

    def get_queryset(self):
        return Task.objects.filter(user=self.request.user)


    def perform_create(self,serializer):
        serializer.save(user = self.request.user) 

    def post(self, request, *args, **kwargs):
        print(self.request.user)
        print(self.request)
        return super().post(request, *args, **kwargs)

class TaskApiDetailView(RetrieveUpdateDestroyAPIView):
    #permission_classes = [IsAuthenticatedOrReadOnly]
    #authentication_classes = [SessionAuthentication]
    serializer_class = TaskSerializer

    def get_queryset(self):
        return Task.objects.filter(user=self.request.user)


