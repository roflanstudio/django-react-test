�������� ��� ���������� ����������� Django � React
 
 ��� �������� ������� �� Windows:
 
 - ������� � ����� ����������� ```$ cd Django-React-Test```
 
 - ���������� ���������� ����� ```$ pip install virtualenv```
 
 - ������� ����� ```$ virtualenv env``` 
 
 - ������������ ����� ```$ env\Scripts\activate```
  
 - ���������� ����������� python ```$ pip install -r Django-React-Test\requirements.txt```
 
 - ���������� ����������� js ```$ npm install```

 - ������� ����������� js ```$ npm runscript build```
 
 - ��������� ������ ```$ python Django-React-Test\manage.py runserver```
